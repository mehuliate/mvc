<?php
//DATABASE
define('DB_HOST', 'localhost');
define('DB_USER', '_YOUR_USER_');
define('DB_PASS', '_YOUR_PASS_');
define('DB_NAME', '_YOUR_DB_');

//APPROOT
define('APPROOT', dirname(dirname(__FILE__)));

//URLROOT
define('URLROOT', '_URL_');

//Site name
define('SITENAME', '_NAME_SITE_');

