<?php
/*
* Base Controller
* load model dan view
*/

class Controller {

    //load model
    public function model($model)
    {
        //require Model file
        require_once '../app/models/' . $model . '.php';

        //instatiate model
        return new $model();
    }

    //Load View
    public function view($view, $data = [])
    {
        //cek view file
        if (file_exists('../app/views/'. $view . '.php')) {
            require_once '../app/views/'. $view . '.php';
        } else {
            die('view does not exist');
        }
    }
}