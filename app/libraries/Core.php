<?php
/*
* App Core Class
* Membuat URL dan Load core controller
* format URL - /controller/method/params
*/

class Core
{
    protected $currentController = 'Pages';
    protected $currentMethod = 'index';
    protected $params = [];

    public function __construct(){

    // print_r($this->getUrl());
        $url = $this->getUrl();

        //lihat controller di index 0
        if(file_exists('../app/controllers/'. ucwords($url[0]).'.php')){
            //set as controller
            $this->currentController = ucwords($url[0]);
            //unset
            unset($url[0]);
        }

        //require controller nya
        require_once '../app/controllers/'. $this->currentController . '.php';

        //instansiasi class controller
        $this->currentController = new $this->currentController;

        //jika ada methode di url kedua
        if (isset($url[1])) {
            //cek jika methode ada di class controller
            if (method_exists($this->currentController, $url[1])) {
                $this->currentMethod = $url[1];
                //unset
                unset($url[1]);
            }
        }

        //get params
        $this->params = $url ? array_values($url) : [] ;

        //call back with array params
        call_user_func_array([$this->currentController, $this->currentMethod], $this->params);

    }

    public function getUrl()
    {
        if (isset($_GET['url'])) {
            $url = $_GET['url'];
            $url = ltrim($url, '/');
            $url = rtrim($url, '/');
            $url = filter_var($url, FILTER_SANITIZE_URL);
            $url = explode('/', $url);

            return $url;
        }
    }
}