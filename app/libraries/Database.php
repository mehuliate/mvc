<?php
/*
* PDO Database class
* Connect to database
* Crate prepare statement
* Bind value
* Return rows and results
*/

class Database {
    private $host = DB_HOST;
    private $user = DB_USER;
    private $pass = DB_PASS;
    private $dbname = DB_NAME;

    private $dbh;
    private $stmt;
    private $error;

    public function __construct()
    {
        //set DSN
        $dsn = 'mysql:host='. $this->host . ';dbname=' . $this->dbname;

        $options = [
            PDO::ATTR_PERSISTENT => true,
            PDO::ATTR_ERRMODE =>PDO::ERRMODE_EXCEPTION
        ];

        //create PDO instance
        try{
            $this->dbh = new PDO($dsn, $this->user, $this->pass, $options);
        } catch(PDOexception $e){
            $this->error = $e->getMessage();
            echo $this->error;
        }
    }

    //prepare statement query
    public function query($sql)
    {
        $this->stmt = $this->dbh->prepare($sql);
    }

    //bind values
    public function bind($param, $value, $type = null)
    {
        if(is_null($type))
        {
            switch (is_null($type)) {
                case is_int($value):
                    $type = PDO::PARAM_INT;
                    break;
                case is_bool($value):
                    $type = PDO::PARAM_BOOL;
                    break;
                case is_null($value):
                    $type = PDO::PARAM_NULL;
                    break;
                default:
                    $type = PDO::PARAM_STR;
            }
        }

        $this->stmt->bindValue($param, $value, $type);
    }

    //excecute the prepared statement
    public function excecute()
    {
        return $this->stmt->execute();
    }

    //hasil sebagai array objects
    public function resultSet()
    {
        $this->excecute();
        return $this->stmt->fetchAll(PDO::FETCH_OBJ);
    }

    //hasil sebagai objects
    public function single()
    {
        $this->excecute();
        return $this->stmt->fetch(PDO::FETCH_OBJ);
    }

    //hasil sebagai count
    public function rowCount()
    {
        return $this->stmt->rowCount();
    }
}